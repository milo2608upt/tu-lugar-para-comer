<?php
session_start();
use proyect\Model\Restaurantes;
include 'Model/Conexion.php';
include 'Model/Restaurantes.php';
    class restaurantesController{

        function __construct() {
           
        }

        function nuevo(){
            require_once "Views/usuario/restaurante.php";
        }

        function reservar(){
            require_once "Views/usuario/reservar.php";
        }

         function insertar(){
            $restaurantes=new Restaurantes();
           	$restaurantes->nombre=$_POST["nombre"];
            $restaurantes->direccion=$_POST["direccion"];
            $restaurantes->contacto=$_POST["contacto"];
            move_uploaded_file($_FILES["imagen"]["tmp_name"],"img/".$_FILES["imagen"]["name"]);
            $restaurantes->imagen="img/".$_FILES["imagen"]["name"];
            $restaurantes->horarios=$_POST["horario"];
            $restaurantes->tipo_comida=$_POST["comida"];
            
            if($_POST["comida"]==""||$_POST["nombre"]==""||$_POST["direccion"]==""||$_POST["contacto"]==""||$_POST["horario"]==""){
                $_SESSION["flash"] = "Campos Vacios";
                header("Location:?controller=restaurantes&action=nuevo");
            }else{
                //if($restaurantes==true){
                    $restaurantes->insert();
                header("Location:?controller=usuarios&action=verres");
                //}else{
                //    echo json_encode(["estatus"=>"error","usuario"=>"error"]);
               // }
            }
        }
         function buscar(){
            $id=$_POST["id"];
            if($id!=""){
                $res=Restaurantes::find($id);
                $_SESSION["idres"]=$res["Id"];
                $_SESSION["nomres"]=$res["Nombre"];
                header("Location:?controller=restaurantes&action=reservar");
            }else{
                echo "error sin paraetros";
            }
        }

        function buscarnombre(){
            $nombre=$_POST["nombre"];
            if($nombre!=""){
                $res=Restaurantes::find($nombre);
                if($res["Nombre"]!=$nombre){
                    echo "no existe usuario";
                
                }else{
                    echo json_encode($res);
                }
            }else{
                echo json_encode(["estatus"=>"success","msj"=>"error"]);
            }
        }

        function read(){
            $res=Restaurantes::selectall();
            echo json_encode(["estado"=>"true","datos"=>$res]);
        }

         function actualizar(){
           $restaurantes=new Restaurantes();
           $restaurantes->id=$_POST["id"];
           	$restaurantes->nombre=$_POST["nombre"];
            $restaurantes->direccion=$_POST["direccion"];
            $restaurantes->contacto=$_POST["contacto"];
            $restaurantes->imagen="foto.png";
            $restaurantes->mesas_disponibles=$_POST["mesas_disponibles"];
            $restaurantes->horarios=$_POST["horario"];
            $restaurantes->calificacion=$_POST["calificacion"];
            $restaurantes->tipo_comida=$_POST["comida"];
            if($_POST["comida"]==""||$_POST["nombre"]==""||$_POST["direccion"]==""||$_POST["contacto"]==""||$_POST["mesas_disponibles"]==""||$_POST["horario"]==""||$_POST["calificacion"]==""||$_POST["id"]==""){
                echo json_encode(["estatus"=>"error","usuario"=>"error"]);
            }else{
                if($restaurantes==true){
                    $restaurantes->update();
                    echo json_encode(["estatus"=>"success","restaurante"=>$restaurantes]);
                }else{
                    echo json_encode(["estatus"=>"error","msj"=>"error"]);
                }
            }
        }


        function eliminar(){
            $id=$_POST["id"]; 
            if($_POST["id"]==""){
                echo json_encode(["estatus"=>"error","msj"=>"error"]);
            }else{
                $res=Restaurantes::delete($id);
                header("Location:?controller=usuarios&action=verres");
            }
        }
    }
 ?>