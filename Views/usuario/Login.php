<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="../../../Tu_Lugar_Para_Comer/img/reserva.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../../../proyect/css/stylesLogin.css">
    <title>LOGIN</title>
</head>
<body>
<div id="wrapper">

    <div class="form-container">

        <span class="form-heading">Iniciar Sesion</span>
        <form action="?controller=usuarios&action=validar" method="POST">
            <?php
            if(isset($_SESSION["flash"])) {
                echo "<p class='msj alert alert-primary'>".$_SESSION["flash"]."</p>";
            }
            unset($_SESSION["flash"]);
            ?>
            <div class="input-group">
                <ion-icon class="i"name="person-outline"></ion-icon>
                <input name="correo" id="correo" type="text" placeholder="Correo">
                <span class="bar"></span>
            </div>
            <div class="input-group">
                <ion-icon class="i" name="lock-closed-outline"></ion-icon>
                <input name="contraseña" id="contraseña" type="password" placeholder="Contraseña">
                <span class="bar"></span>
            </div>
            <div class="input-group">
                <button>
                    <ion-icon name="log-in-outline"></ion-icon>
                </button>
                <a href="?controller=Usuarios&action=registrar">
                    <ion-icon name="person-add-outline"></ion-icon>
                </a>
            </div>
        </form>
    </div>
</div>
</body>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
</html>
