<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../../../proyect/css/styleRegistro.css">
    <title>Login - Tu Lugar Para Comer</title>
</head>
<body>
<section class="contact-box">
    <div class="row no-gutters bg-dark">
        <div class="col-xl-5 col-lg-12 register-bg">

        </div>
        <div class="col-xl-7 col-lg-12 d-flex">
            <div class="container align-self-center p-6">
                <h1 class="font-weight-bold">Crea un restaurate</h1>
                <p class="text-muted mb-5">Ingresa la siguiente informacion para registrar un restaurante.</p>
                <form action="?controller=restaurantes&action=insertar" method="POST" enctype="multipart/form-data">
                    <?php
                    if(isset($_SESSION["flash"])) {
                        echo "<p class='msj alert alert-primary'>".$_SESSION["flash"]."</p>";
                        unset($_SESSION["flash"]);
                    }

                    ?>
                    <div class="form-group mb-3">
                        <label class="font-weight-bold">Nombre<span class="text-danger">*</span></label>
                        <input required type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre del Restaurante">
                    </div>
                    <div class="form-group mb-3">
                        <label class="font-weight-bold">Direccion<span class="text-danger">*</span></label>
                        <input required type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingresa la Direccion">
                    </div>
                    <div class="form-group mb-3">
                        <label class="font-weight-bold">Contacto<span class="text-danger">*</span></label>
                        <input required type="tel"name="contacto" id="contacto" class="form-control" placeholder="Ingresa un Numero Telefonico">
                    </div>
                    <div class="form-group mb-3">
                        <label class="font-weight-bold">Horario<span class="text-danger">*</span></label>
                        <input required type="time"name="horario" id="horario" class="form-control" placeholder="Ingresa el Horario">
                    </div>
                    <div class="form-group mb-3">
                        <label class="font-weight-bold">Comida<span class="text-danger">*</span></label>
                        <input required type="text"name="comida" id="comida" class="form-control" placeholder="Tipo de Comida">
                    </div>
                    <div class="form-group mb-3">
                        <label class="font-weight-bold">Imagen<span class="text-danger">*</span></label>
                        <input required type="file"name="imagen" id="imagen" class="form-control">
                    </div>
                    <input class="btn btn-primary" type="submit" value="Registrar">
                    <a href="?controller=usuarios&action=init" class="btn btn-light letter">Regresar</a>
                </form>
            </div>
        </div>
    </div>
</section>
</body>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
</html>
